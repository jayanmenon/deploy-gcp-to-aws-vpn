# Define a vpc
resource "aws_vpc" "vpc_name" {
  cidr_block = "${var.aws_cidr_block}"
  tags = {
    Name = "${var.aws_name_tag}: VPC"
  }
}

# Internet gateway for the public subnet
resource "aws_internet_gateway" "awsvpc_ig" {
  vpc_id = "${aws_vpc.vpc_name.id}"
  tags = {
    Name = "${var.aws_name_tag}: Internet Gateway"
  }
}

# Public subnet Primary
resource "aws_subnet" "awsvpc_subnet" {
  vpc_id            = "${aws_vpc.vpc_name.id}"
  cidr_block        = "${var.aws_subnet_cidr}"
  availability_zone = "us-east-2a"
  tags = {
    Name = "${var.aws_name_tag}: Subnet"
  }
}

# Routing table for public subnet
resource "aws_route_table" "awsvpc_rt" {
  vpc_id = "${aws_vpc.vpc_name.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.awsvpc_ig.id}"
  }
  tags = {
    Name = "${var.aws_name_tag}: Route Table"
  }
}

# Associate the routing table to public subnet
resource "aws_route_table_association" "awsvpc_subnet" {
  subnet_id      = "${aws_subnet.awsvpc_subnet.id}"
  route_table_id = "${aws_route_table.awsvpc_rt.id}"
}

# ECS Instance Security group
resource "aws_security_group" "awsvpc_sg" {
  vpc_id = "${aws_vpc.vpc_name.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    # allow all traffic fromm VPN
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["${var.gcp_subnet_cidr}"]
  }

  egress {
    # allow all traffic outbound
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.aws_name_tag}: Security Group"
  }
}


