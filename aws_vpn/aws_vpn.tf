data "terraform_remote_state" "gcp_data" {
  backend = "local"
  config = {
    path = "../gcpinit/terraform.tfstate"
  }
}

resource "aws_customer_gateway" "awsvpc_cg" {
  bgp_asn    = 65000
  ip_address = data.terraform_remote_state.gcp_data.outputs.gcp_vpn_connection_ip
  type       = "ipsec.1"

  tags = {
    Name = "${var.aws_name_tag}: Customer Gateway"
  }
}

resource "aws_vpn_gateway" "aws_vpn_gw" {
  vpc_id = "${aws_vpc.vpc_name.id}"

  tags = {
    Name = "${var.aws_name_tag}: VPN Gateway"
  }
}

resource "aws_vpn_connection" "aws_vpn" {
  vpn_gateway_id        = "${aws_vpn_gateway.aws_vpn_gw.id}"
  customer_gateway_id   = "${aws_customer_gateway.awsvpc_cg.id}"
  type                  = "ipsec.1"
  tunnel1_preshared_key = "${var.vpn_preshared_key}"
  tunnel2_preshared_key = "${var.vpn_preshared_key}"
  static_routes_only    = true
  tags = {
    Name = "${var.aws_name_tag}: VPN Tunnel"
  }
}

resource "aws_vpn_connection_route" "gcp" {
  destination_cidr_block = "${var.gcp_subnet_cidr}"
  vpn_connection_id      = "${aws_vpn_connection.aws_vpn.id}"
}
