variable "gcp_subnet_cidr" {
  description = "GCP Subnet CIDR"
  default     = "10.16.10.0/24"
}

# AWS variables

variable "aws_access_key_id" {
  description = "AWS access key"
}

variable "aws_secret_access_key" {
  description = "AWS secret access key"
}

variable "aws_region" {
  description = "AWS region"
  default     = "us-east-2"
}

variable "aws_cidr_block" {
  description = "AWS VPC CIDR Block"
  default     = "172.16.0.0/16"
}

variable "aws_subnet_cidr" {
  description = "AWS Subnet CIDR"
  default     = "172.16.10.0/24"
}

variable "aws_name_tag" {
  description = "Name tag for all resources"
}

# Common variables for both

variable "vpn_preshared_key" {
  description = "preshared key used for tunnels 1 and 2"
}

