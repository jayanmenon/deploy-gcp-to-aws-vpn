output "vpn_connection_tunnel1_address" {
  value = "${aws_vpn_connection.aws_vpn.tunnel1_address}"
}

output "vpn_connection_tunnel2_address" {
  value = "${aws_vpn_connection.aws_vpn.tunnel2_address}"
}
