**This repository contains terraform code to create a site to site VPN between AWS and GCP**

This is done in three steps:
1. Reserve a new static IP in GCP and output it to the state file
2. Use the remote IP from the previous step, create an AWS VPC, Subnet, Gateways and VPN tunnels. The tunnel IP addresses will then be output to state files.
3. Use the tunnel IP addresses from the previous step to create VPN tunnels from GCP back to AWS.

---
## How to manually run this process

Clone the repository locally

```
git clone git@bitbucket.org:jayanmenon/deploy-gcp-to-aws-vpn.git
```

On the root directory, run `terraform init` to download the provider plugins.

Change to gcpinit folder.

``` 
cd gcpinit
```
Create a terraform.tfvars with the following variables with appropriate values

```
gcp_project           = "<gcp project>"
gcp_region            = "<gcp region>"
gcp_svcacct_json      = "<json file for service account>"
```

Run terraform to reserve a static IP 

```
terraform plan
terraform apply --auto-approve
```

Change to aws_vpn folder.

``` 
cd ../aws_vpn
```
Create a terraform.tfvars with the following variables with appropriate values

```
aws_access_key_id     = "<access key>"
aws_secret_access_key = "<secret>"
aws_name_tag          = "<common string to identify all resources created>"
vpn_preshared_key     = "<generate a random key for vpn and paste here>"
```

Run terraform to complete the AWS side of the configuration

```
terraform plan
terraform apply --auto-approve
```

Change to gcp_vpn folder.

``` 
cd ../gcp_vpn
```
Create a terraform.tfvars with the following variables with appropriate values

```
gcp_project       = "<gcp project name>"
gcp_vpc           = "<gcp vpc name>"
gcp_svcacct_json  = "<json file for service account>"
vpn_preshared_key = "<same preshared key used above for aws>"
```

Run terraform to complete the GCP side of the configuration

```
terraform plan
terraform apply --auto-approve
```


