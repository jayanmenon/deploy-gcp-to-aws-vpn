# GCP variables

variable "gcp_project" {
  description = "GCP Project"
}

variable "gcp_region" {
  description = "GCP region"
  default     = "us-central1"
}

variable "gcp_svcacct_json" {
  description = "GCP Service Account json"
}

