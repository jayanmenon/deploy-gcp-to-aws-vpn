provider "google" {
  credentials = file(var.gcp_svcacct_json)
  project     = var.gcp_project
  region      = var.gcp_region
}

