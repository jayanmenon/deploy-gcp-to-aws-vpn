data "terraform_remote_state" "aws_data" {
  backend = "local"
  config = {
    path = "../aws_vpn/terraform.tfstate"
  }
}

resource "google_compute_network" "gcp_vpc" {
  name                    = var.gcp_vpc
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "gcp_subnet" {
  name          = "subnet-primary"
  ip_cidr_range = var.gcp_subnet_cidr
  network       = google_compute_network.gcp_vpc.self_link
  region        = var.gcp_region
}

resource "google_compute_firewall" "gcp_vpc_allow_internal" {
  name    = "gcp-vpc-allow-internal"
  network = google_compute_network.gcp_vpc.self_link

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }

  allow {
    protocol = "icmp"
  }

  source_ranges = [var.gcp_subnet_cidr]
}

resource "google_compute_firewall" "gcp_vpc_allow_vpn" {
  name    = "gcp-vpc-allow-vpn"
  network = google_compute_network.gcp_vpc.self_link

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }

  allow {
    protocol = "icmp"
  }

  source_ranges = [var.aws_subnet_cidr]
}

