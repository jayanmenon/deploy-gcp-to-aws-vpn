# GCP variables

variable "gcp_project" {
  description = "GCP Project"
}

variable "gcp_region" {
  description = "GCP region"
  default     = "us-central1"
}

variable "gcp_svcacct_json" {
  description = "GCP Service Account json"
}

variable "gcp_vpc" {
  description = "GCP VPC"
}

variable "gcp_subnet_cidr" {
  description = "GCP Subnet CIDR"
  default     = "10.16.10.0/24"
}

# AWS variables

variable "aws_subnet_cidr" {
  description = "AWS Subnet CIDR"
  default     = "172.16.10.0/24"
}

# Common variables for both

variable "vpn_preshared_key" {
  description = "preshared key used for tunnels 1 and 2"
}

