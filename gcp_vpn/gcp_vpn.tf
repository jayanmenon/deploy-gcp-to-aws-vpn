data "terraform_remote_state" "gcp_data" {
  backend = "local"
  config = {
    path = "../gcpinit/terraform.tfstate"
  }
}

resource "google_compute_vpn_gateway" "aws_gateway" {
  name    = "aws-gateway"
  network = google_compute_network.gcp_vpc.self_link
  region  = var.gcp_region
}

resource "google_compute_forwarding_rule" "fr_esp" {
  name        = "fr-esp"
  region      = var.gcp_region
  ip_protocol = "ESP"
  ip_address  = data.terraform_remote_state.gcp_data.outputs.gcp_vpn_connection_ip
  target      = google_compute_vpn_gateway.aws_gateway.self_link
}

resource "google_compute_forwarding_rule" "fr_udp500" {
  name        = "fr-udp500"
  region      = var.gcp_region
  ip_protocol = "UDP"
  port_range  = "500"
  ip_address  = data.terraform_remote_state.gcp_data.outputs.gcp_vpn_connection_ip
  target      = google_compute_vpn_gateway.aws_gateway.self_link
}

resource "google_compute_forwarding_rule" "fr_udp4500" {
  name        = "fr-udp4500"
  region      = var.gcp_region
  ip_protocol = "UDP"
  port_range  = "4500"
  ip_address  = data.terraform_remote_state.gcp_data.outputs.gcp_vpn_connection_ip
  target      = google_compute_vpn_gateway.aws_gateway.self_link
}

resource "google_compute_vpn_tunnel" "tunnel1" {
  name               = "aws-tunnel1"
  region             = var.gcp_region
  peer_ip            = data.terraform_remote_state.aws_data.outputs.vpn_connection_tunnel1_address
  ike_version        = "1"
  shared_secret      = var.vpn_preshared_key
  target_vpn_gateway = google_compute_vpn_gateway.aws_gateway.self_link

  local_traffic_selector = [
    var.gcp_subnet_cidr,
  ]
  remote_traffic_selector = [
    var.aws_subnet_cidr,
  ]

  depends_on = ["google_compute_forwarding_rule.fr_udp500",
    "google_compute_forwarding_rule.fr_udp4500",
    "google_compute_forwarding_rule.fr_esp",
  ]
}

resource "google_compute_vpn_tunnel" "tunnel2" {
  name               = "aws-tunnel2"
  region             = var.gcp_region
  peer_ip            = data.terraform_remote_state.aws_data.outputs.vpn_connection_tunnel2_address
  ike_version        = "1"
  shared_secret      = var.vpn_preshared_key
  target_vpn_gateway = google_compute_vpn_gateway.aws_gateway.self_link

  local_traffic_selector = [
    var.gcp_subnet_cidr,
  ]
  remote_traffic_selector = [
    var.aws_subnet_cidr,
  ]

  depends_on = ["google_compute_forwarding_rule.fr_udp500",
    "google_compute_forwarding_rule.fr_udp4500",
    "google_compute_forwarding_rule.fr_esp",
  ]
}

# route through tunnel 1 takes precedence with lower priority
resource "google_compute_route" "aws_tunnel1_route" {
  name                = "aws-tunnel1-route"
  dest_range          = var.aws_subnet_cidr
  network             = google_compute_network.gcp_vpc.self_link
  next_hop_vpn_tunnel = google_compute_vpn_tunnel.tunnel1.self_link
  priority            = 90
}

resource "google_compute_route" "aws_tunnel2_route" {
  name                = "aws-tunnel2-route"
  dest_range          = var.aws_subnet_cidr
  network             = google_compute_network.gcp_vpc.self_link
  next_hop_vpn_tunnel = google_compute_vpn_tunnel.tunnel2.self_link
  priority            = 100
}

